﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MVC.Models
{
    public class mvcFAQModel
    {
        public int FAQ_ID { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string FAQ_Questions { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string FAQ_Answer { get; set; }
    }
}