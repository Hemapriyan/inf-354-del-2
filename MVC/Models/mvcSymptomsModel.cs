﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class mvcSymptomsModel
    {
        public int Symptoms_ID { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string Symptoms_Description { get; set; }
        public Nullable<int> Disease_ID { get; set; }

        public virtual mvcDiseaseModel Disease { get; set; }
    }
}