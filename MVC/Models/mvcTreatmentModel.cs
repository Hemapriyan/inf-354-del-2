﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class mvcTreatmentModel
    {
        public int Treatment_ID { get; set; }
        [Required(ErrorMessage = "This field is required")]

        public string Treatment_Description { get; set; }
        public Nullable<int> Disease_ID { get; set; }

        public virtual mvcDiseaseModel Disease { get; set; }
    }
}