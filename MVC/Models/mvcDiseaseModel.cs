﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class mvcDiseaseModel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public mvcDiseaseModel()
        {
            this.Disease_Prevention = new HashSet<mvcPreventionModel>();
            this.Disease_Symptoms = new HashSet<mvcSymptomsModel>();
            this.Disease_Treatment = new HashSet<mvcTreatmentModel>();
        }

        public int Disease_ID { get; set; }
        [Required(ErrorMessage ="This field is required")]
        public string Disease_Name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<mvcPreventionModel> Disease_Prevention { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<mvcSymptomsModel> Disease_Symptoms { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<mvcTreatmentModel> Disease_Treatment { get; set; }

    }
}