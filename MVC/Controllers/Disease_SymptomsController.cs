﻿using MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class Disease_SymptomsController : Controller
    {
        // GET: Disease_Symptoms
        public ActionResult Index()
        {
            IEnumerable<mvcSymptomsModel> emplist;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Disease_Symptoms").Result;
            emplist = response.Content.ReadAsAsync<IEnumerable<mvcSymptomsModel>>().Result;

            return View(emplist);
        }

        public ActionResult AddOrEdit(int id = 0)
        {
            if (id == 0)
                return View(new mvcSymptomsModel());
            else
            {
                HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Disease_Symptoms/" + id.ToString()).Result;
                return View(response.Content.ReadAsAsync<mvcSymptomsModel>().Result);
            }
        }

        [HttpPost]
        public ActionResult AddOrEdit(mvcSymptomsModel emp)
        {
            if (emp.Symptoms_ID == 0)
            {
                emp.Disease_ID = 1;

                HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("Disease_Symptoms", emp).Result;
                TempData["SuccessMessage"] = "Saved Successfully";
            }
            else
            {
                emp.Disease_ID = 1;

                HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("Disease_Symptoms/" + emp.Symptoms_ID, emp).Result;
                TempData["SuccessMessage"] = "Updated Successfully";
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            HttpResponseMessage response = GlobalVariables.WebApiClient.DeleteAsync("Disease_Symtpoms/" + id.ToString()).Result;
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("Index");
        }
    }
}