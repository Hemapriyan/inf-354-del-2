﻿using MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class Disease_TreatmentController : Controller
    {
        // GET: Disease_Treatment
        public ActionResult Index()
        {
            IEnumerable<mvcTreatmentModel> emplist;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Disease_Treatment").Result;
            emplist = response.Content.ReadAsAsync<IEnumerable<mvcTreatmentModel>>().Result;

            return View(emplist);
        }
        public ActionResult AddOrEdit(int id = 0)
        {
            if (id == 0)
                return View(new mvcTreatmentModel());
            else
            {
                HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Disease_Treatment/" + id.ToString()).Result;
                return View(response.Content.ReadAsAsync<mvcTreatmentModel>().Result);
            }
        }

        [HttpPost]
        public ActionResult AddOrEdit(mvcTreatmentModel emp)
        {
            if (emp.Treatment_ID == 0)
            {
                emp.Disease_ID = 1;
                HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("Disease_Treatment", emp).Result;
                TempData["SuccessMessage"] = "Saved Successfully";
            }
            else
            {
                emp.Disease_ID = 1;
                HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("Disease_Treatment/" + emp.Treatment_ID, emp).Result;
                TempData["SuccessMessage"] = "Updated Successfully";
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            HttpResponseMessage response = GlobalVariables.WebApiClient.DeleteAsync("Disease_Treatment/" + id.ToString()).Result;
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("Index");
        }
    }
}