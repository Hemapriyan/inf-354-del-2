﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using MVC.Models;

namespace MVC.Controllers
{
    public class FAQController : Controller
    {
        // GET: FAQ
        public ActionResult Index()
        {

            IEnumerable<mvcFAQModel> emplist;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("FAQ").Result;
            emplist = response.Content.ReadAsAsync<IEnumerable<mvcFAQModel>>().Result;

            return View(emplist);
        }

        public ActionResult AddOrEdit(int id = 0)
        {
            if (id == 0)
                return View(new mvcFAQModel());
            else
            {
                HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("FAQ/" + id.ToString()).Result;
                return View(response.Content.ReadAsAsync<mvcFAQModel>().Result);
            }
        }

        [HttpPost]
        public ActionResult AddOrEdit(mvcFAQModel emp)
        {
            if (emp.FAQ_ID == 0)
            {
                HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("FAQ", emp).Result;
                TempData["SuccessMessage"] = "Saved Successfully";
            }
            else
            {
                HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("FAQ/" + emp.FAQ_ID, emp).Result;
                TempData["SuccessMessage"] = "Updated Successfully";
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            HttpResponseMessage response = GlobalVariables.WebApiClient.DeleteAsync("FAQ/" + id.ToString()).Result;
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("Index");
        }



    }
}