﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication7API.Models;

namespace WebApplication7API.Controllers
{
    public class DiseaseController : ApiController
    {
        private Malaria_DiseaseEntities db = new Malaria_DiseaseEntities();

        // GET: api/Disease
        public IQueryable<Disease> GetDiseases()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return db.Diseases;
        }

        // GET: api/Disease/5
        [ResponseType(typeof(Disease))]
        public IHttpActionResult GetDisease(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            Disease disease = db.Diseases.Find(id);
            if (disease == null)
            {
                return NotFound();
            }

            return Ok(disease);
        }

        // PUT: api/Disease/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDisease(int id, Disease disease)
        {
            db.Configuration.ProxyCreationEnabled = false;
           

            if (id != disease.Disease_ID)
            {
                return BadRequest();
            }

            db.Entry(disease).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DiseaseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Disease
        [ResponseType(typeof(Disease))]
        public IHttpActionResult PostDisease(Disease disease)
        {
            db.Configuration.ProxyCreationEnabled = false;
            

            db.Diseases.Add(disease);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (DiseaseExists(disease.Disease_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = disease.Disease_ID }, disease);
        }

        // DELETE: api/Disease/5
        [ResponseType(typeof(Disease))]
        public IHttpActionResult DeleteDisease(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            Disease disease = db.Diseases.Find(id);
            if (disease == null)
            {
                return NotFound();
            }

            db.Diseases.Remove(disease);
            db.SaveChanges();

            return Ok(disease);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DiseaseExists(int id)
        {
            return db.Diseases.Count(e => e.Disease_ID == id) > 0;
        }
    }
}