﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication7API.Models;

namespace WebApplication7API.Controllers
{
    public class Audit_LogController : ApiController
    {
        private Malaria_DiseaseEntities db = new Malaria_DiseaseEntities();

        // GET: api/Audit_Log
        public IQueryable<Audit_Log> GetAudit_Logs()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return db.Audit_Logs;
        }

        // GET: api/Audit_Log/5
        [ResponseType(typeof(Audit_Log))]
        public IHttpActionResult GetAudit_Log(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            Audit_Log audit_Log = db.Audit_Logs.Find(id);
            if (audit_Log == null)
            {
                return NotFound();
            }

            return Ok(audit_Log);
        }

        // PUT: api/Audit_Log/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAudit_Log(int id, Audit_Log audit_Log)
        {
            db.Configuration.ProxyCreationEnabled = false;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != audit_Log.Login_ID)
            {
                return BadRequest();
            }

            db.Entry(audit_Log).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Audit_LogExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Audit_Log
        [ResponseType(typeof(Audit_Log))]
        public IHttpActionResult PostAudit_Log(Audit_Log audit_Log)
        {
            db.Configuration.ProxyCreationEnabled = false;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Audit_Logs.Add(audit_Log);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = audit_Log.Login_ID }, audit_Log);
        }

        // DELETE: api/Audit_Log/5
        [ResponseType(typeof(Audit_Log))]
        public IHttpActionResult DeleteAudit_Log(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            Audit_Log audit_Log = db.Audit_Logs.Find(id);
            if (audit_Log == null)
            {
                return NotFound();
            }

            db.Audit_Logs.Remove(audit_Log);
            db.SaveChanges();

            return Ok(audit_Log);
        }

        protected override void Dispose(bool disposing)
        {
            db.Configuration.ProxyCreationEnabled = false;
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Audit_LogExists(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return db.Audit_Logs.Count(e => e.Login_ID == id) > 0;
        }
    }
}