﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication7API.Models;

namespace WebApplication7API.Controllers
{
    public class Disease_TreatmentController : ApiController
    {
        private Malaria_DiseaseEntities db = new Malaria_DiseaseEntities();

        // GET: api/Disease_Treatment
        public IQueryable<Disease_Treatment> GetDisease_Treatment()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return db.Disease_Treatment;
        }

        // GET: api/Disease_Treatment/5
        [ResponseType(typeof(Disease_Treatment))]
        public IHttpActionResult GetDisease_Treatment(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            Disease_Treatment disease_Treatment = db.Disease_Treatment.Find(id);
            if (disease_Treatment == null)
            {
                return NotFound();
            }

            return Ok(disease_Treatment);
        }

        // PUT: api/Disease_Treatment/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDisease_Treatment(int id, Disease_Treatment disease_Treatment)
        {
            db.Configuration.ProxyCreationEnabled = false;

            if (id != disease_Treatment.Treatment_ID)
            {
                return BadRequest();
            }

            db.Entry(disease_Treatment).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Disease_TreatmentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Disease_Treatment
        [ResponseType(typeof(Disease_Treatment))]
        public IHttpActionResult PostDisease_Treatment(Disease_Treatment disease_Treatment)
        {
            db.Configuration.ProxyCreationEnabled = false;

            db.Disease_Treatment.Add(disease_Treatment);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = disease_Treatment.Treatment_ID }, disease_Treatment);
        }

        // DELETE: api/Disease_Treatment/5
        [ResponseType(typeof(Disease_Treatment))]
        public IHttpActionResult DeleteDisease_Treatment(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            Disease_Treatment disease_Treatment = db.Disease_Treatment.Find(id);
            if (disease_Treatment == null)
            {
                return NotFound();
            }

            db.Disease_Treatment.Remove(disease_Treatment);
            db.SaveChanges();

            return Ok(disease_Treatment);
        }

        protected override void Dispose(bool disposing)
        {
            db.Configuration.ProxyCreationEnabled = false;
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Disease_TreatmentExists(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return db.Disease_Treatment.Count(e => e.Treatment_ID == id) > 0;
        }
    }
}