﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication7API.Models;

namespace WebApplication7API.Controllers
{
    public class FAQController : ApiController
    {
        private Malaria_DiseaseEntities db = new Malaria_DiseaseEntities();

        // GET: api/FAQ
        public IQueryable<FAQ> GetFAQs()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return db.FAQs;
        }

        // GET: api/FAQ/5
        [ResponseType(typeof(FAQ))]
        public IHttpActionResult GetFAQ(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            FAQ fAQ = db.FAQs.Find(id);
            if (fAQ == null)
            {
                return NotFound();
            }

            return Ok(fAQ);
        }

        // PUT: api/FAQ/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFAQ(int id, FAQ fAQ)
        {
            db.Configuration.ProxyCreationEnabled = false;

            if (id != fAQ.FAQ_ID)
            {
                return BadRequest();
            }

            db.Entry(fAQ).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FAQExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/FAQ
        [ResponseType(typeof(FAQ))]
        public IHttpActionResult PostFAQ(FAQ fAQ)
        {
            db.Configuration.ProxyCreationEnabled = false;

            db.FAQs.Add(fAQ);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = fAQ.FAQ_ID }, fAQ);
        }

        // DELETE: api/FAQ/5
        [ResponseType(typeof(FAQ))]
        public IHttpActionResult DeleteFAQ(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            FAQ fAQ = db.FAQs.Find(id);
            if (fAQ == null)
            {
                return NotFound();
            }

            db.FAQs.Remove(fAQ);
            db.SaveChanges();

            return Ok(fAQ);
        }

        protected override void Dispose(bool disposing)
        {
            db.Configuration.ProxyCreationEnabled = false;
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FAQExists(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return db.FAQs.Count(e => e.FAQ_ID == id) > 0;
        }
    }
}