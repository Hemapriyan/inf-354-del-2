﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication7API.Models;

namespace WebApplication7API.Controllers
{
    public class Disease_SymptomsController : ApiController
    {
        private Malaria_DiseaseEntities db = new Malaria_DiseaseEntities();

        // GET: api/Disease_Symptoms
        public IQueryable<Disease_Symptoms> GetDisease_Symptoms()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return db.Disease_Symptoms;
        }

        // GET: api/Disease_Symptoms/5
        [ResponseType(typeof(Disease_Symptoms))]
        public IHttpActionResult GetDisease_Symptoms(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            Disease_Symptoms disease_Symptoms = db.Disease_Symptoms.Find(id);
            if (disease_Symptoms == null)
            {
                return NotFound();
            }

            return Ok(disease_Symptoms);
        }

        // PUT: api/Disease_Symptoms/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDisease_Symptoms(int id, Disease_Symptoms disease_Symptoms)
        {
            db.Configuration.ProxyCreationEnabled = false;

            if (id != disease_Symptoms.Symptoms_ID)
            {
                return BadRequest();
            }

            db.Entry(disease_Symptoms).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Disease_SymptomsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Disease_Symptoms
        [ResponseType(typeof(Disease_Symptoms))]
        public IHttpActionResult PostDisease_Symptoms(Disease_Symptoms disease_Symptoms)
        {
            db.Configuration.ProxyCreationEnabled = false;

            db.Disease_Symptoms.Add(disease_Symptoms);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = disease_Symptoms.Symptoms_ID }, disease_Symptoms);
        }

        // DELETE: api/Disease_Symptoms/5
        [ResponseType(typeof(Disease_Symptoms))]
        public IHttpActionResult DeleteDisease_Symptoms(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            Disease_Symptoms disease_Symptoms = db.Disease_Symptoms.Find(id);
            if (disease_Symptoms == null)
            {
                return NotFound();
            }

            db.Disease_Symptoms.Remove(disease_Symptoms);
            db.SaveChanges();

            return Ok(disease_Symptoms);
        }

        protected override void Dispose(bool disposing)
        {
            db.Configuration.ProxyCreationEnabled = false;
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Disease_SymptomsExists(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return db.Disease_Symptoms.Count(e => e.Symptoms_ID == id) > 0;
        }
    }
}