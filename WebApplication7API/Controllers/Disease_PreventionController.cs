﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication7API.Models;

namespace WebApplication7API.Controllers
{
    public class Disease_PreventionController : ApiController
    {
        private Malaria_DiseaseEntities db = new Malaria_DiseaseEntities();

        // GET: api/Disease_Prevention
        public IQueryable<Disease_Prevention> GetDisease_Prevention()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return db.Disease_Prevention;
        }

        // GET: api/Disease_Prevention/5
        [ResponseType(typeof(Disease_Prevention))]
        public IHttpActionResult GetDisease_Prevention(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;

            Disease_Prevention disease_Prevention = db.Disease_Prevention.Find(id);
            if (disease_Prevention == null)
            {
                return NotFound();
            }

            return Ok(disease_Prevention);
        }

        // PUT: api/Disease_Prevention/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDisease_Prevention(int id, Disease_Prevention disease_Prevention)
        {
            db.Configuration.ProxyCreationEnabled = false;

            
            if (id != disease_Prevention.Prevention_ID)
            {
                return BadRequest();
            }

            db.Entry(disease_Prevention).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Disease_PreventionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Disease_Prevention
        [ResponseType(typeof(Disease_Prevention))]
        public IHttpActionResult PostDisease_Prevention(Disease_Prevention disease_Prevention)
        {
            db.Configuration.ProxyCreationEnabled = false;

            

            db.Disease_Prevention.Add(disease_Prevention);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = disease_Prevention.Prevention_ID }, disease_Prevention);
        }

        // DELETE: api/Disease_Prevention/5
        [ResponseType(typeof(Disease_Prevention))]
        public IHttpActionResult DeleteDisease_Prevention(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;

            Disease_Prevention disease_Prevention = db.Disease_Prevention.Find(id);
            if (disease_Prevention == null)
            {
                return NotFound();
            }

            db.Disease_Prevention.Remove(disease_Prevention);
            db.SaveChanges();

            return Ok(disease_Prevention);
        }

        protected override void Dispose(bool disposing)
        {
            db.Configuration.ProxyCreationEnabled = false;

            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Disease_PreventionExists(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;

            return db.Disease_Prevention.Count(e => e.Prevention_ID == id) > 0;
        }
    }
}