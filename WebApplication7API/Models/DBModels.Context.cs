﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication7API.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class Malaria_DiseaseEntities : DbContext
    {
        public Malaria_DiseaseEntities()
            : base("name=Malaria_DiseaseEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Audit_Log> Audit_Logs { get; set; }
        public virtual DbSet<Carrier> Carriers { get; set; }
        public virtual DbSet<Disease> Diseases { get; set; }
        public virtual DbSet<Disease_Prevention> Disease_Prevention { get; set; }
        public virtual DbSet<Disease_Symptoms> Disease_Symptoms { get; set; }
        public virtual DbSet<Disease_Treatment> Disease_Treatment { get; set; }
        public virtual DbSet<FAQ> FAQs { get; set; }
        public virtual DbSet<Job> Jobs { get; set; }
        public virtual DbSet<Region> Regions { get; set; }
        public virtual DbSet<User_Status> User_Status { get; set; }
        public virtual DbSet<tblUser> tblUsers { get; set; }
    }
}
